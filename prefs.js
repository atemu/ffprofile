user_pref("browser.urlbar.placeholderName", "DuckDuckGo");
user_pref("extensions.webextensions.uuids", "{\"uBlock0@raymondhill.net\":\"ublockorigin\",\"{d7742d87-e61d-4b78-b8a1-b469842139fa}\":\"vimium\"}");
user_pref("browser.tabs.warnOnClose", false);
user_pref("browser.startup.page", 3); //restore session on startup
user_pref("browser.ctrlTab.recentlyUsedOrder", false);
user_pref("browser.newtabpage.activity-stream.feeds.snippets", false);
user_pref("browser.newtabpage.activity-stream.feeds.section.highlights", false);
user_pref("browser.aboutConfig.showWarning", false);
user_pref("ui.key.accelKey", 18);
user_pref("permissions.default.desktop-notification", 2);
user_pref("media.autoplay.default", 5); //block audio and video
user_pref("browser.discovery.enabled", false); //don't recommend extensions
user_pref("datareporting.healthreport.uploadEnabled", false); //telemetry
user_pref("app.shield.optoutstudies.enabled", false);
user_pref("browser.download.useDownloadDir", false); //Ask where to save downloads
user_pref("browser.newtabpage.activity-stream.asrouter.userprefs.cfr.addons", false);
user_pref("browser.newtabpage.activity-stream.asrouter.userprefs.cfr.features", false);
user_pref("network.trr.mode", 2);
user_pref("network.trr.uri", "127.0.0.1");
user_pref("extensions.activeThemeID", "firefox-compact-dark@mozilla.org");
user_pref("extensions.update.autoUpdateDefault", false);
user_pref("toolkit.telemetry.reportingpolicy.firstRun", false); //privacy notice popunder
user_pref("browser.startup.homepage_override.mstone", "0.0");
user_pref("datareporting.policy.dataSubmissionPolicyAcceptedVersion", 2);
user_pref("datareporting.policy.dataSubmissionPolicyNotifiedTime", "2147483647000");
